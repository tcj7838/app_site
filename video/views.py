from django.shortcuts import render
from datetime import datetime

# Create your views here.

def course(request, course_no=0):
    course_list=[
        {'name':'李宏毅-機器學習', 'tvcode':'CXgbekl66jc'},
        {'name':'林軒田-機器學習基石', 'tvcode':'nQvpFSMPhr0'},
        {'name':'吳尚鴻-深度學習', 'tvcode':'jqLidqLZbA8'},
        {'name':'莫煩-強化學習', 'tvcode':'NVWBs7b3oGk'},
        {'name':'澎澎-網路爬蟲','tvcode':'9Z9xKWfNo7k'},
    ]

    car_maker = ['SAAB', 'Ford', 'Honda', 'Mazda', 'Nissan','Toyota' ]
    car_list = [ 
        [],
        ['Fiesta', 'Focus', 'Modeo', 'EcoSport', 'Kuga', 'Mustang'],
        ['Fit', 'Odyssey', 'CR-V', 'City', 'NSX'],
        ['Mazda3', 'Mazda5', 'Mazda6', 'CX-3', 'CX-5', 'MX-5'],
        ['Tida', 'March', 'Livina', 'Sentra', 'Teana', 'X-Trail', 'Juke', 'Murano'],
        ['Camry','Altis','Yaris','86','Prius','Vios', 'RAV4', 'Wish']
    ]
    maker = course_no
    maker_name = car_maker[maker]
    cars=car_list[maker]


    now = datetime.now()
    hour = now.timetuple().tm_hour
    course_no = course_no
    course = course_list[course_no]
    return render(request, 'live.html', locals())
