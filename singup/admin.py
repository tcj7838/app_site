from django.contrib import admin
from singup.models import registration_info
# Register your models here.



class RegAdmin(admin.ModelAdmin):
    list_display = ('uid','email','cellphone')

admin.site.register(registration_info,RegAdmin)