## 新的 singup/urls.py 
from django.urls import path, re_path
from singup.views import *

# 127.0.0.1/singup/-----
urlpatterns = [
    path('',listing),
    path('list/', listing),
    re_path(r'^list/([0|1|2|3|4])/$',disp_detail),
    path('about/', about),
]