from django.db import models
from django.db.models.base import Model

# Create your models here.


class registration_info(models.Model):
    dept_select = (
        ('IT', '資訊課'),
        ('HR', '人資課'),
        ('RD', '研發課'),
        ('PR', '公關課')
    )
    uid = models.CharField(max_length=20, unique=True, verbose_name='用户名')
    pwd = models.CharField(max_length=128)
    email= models.EmailField()
    signup_time = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    id_no = models.CharField(max_length=10)
    cellphone = models.CharField(max_length=10)
    address = models.TextField()
    webpage = models.URLField()
    dept = models.CharField(max_length=2, default="IT" , choices = dept_select)
    st = models.BooleanField()

    def __str__(self):
        return  self.email

class product(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    qty = models.IntegerField()

    def __str__(self) -> str:
        return self.name