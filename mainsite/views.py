from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Post
from datetime import datetime

# Create your views here.



def homepage(request):
    posts = Post.objects.all()
    now = datetime.now()
    
    # for count , post in enumerate(posts):
    #     post_list.append("NO.{}".format(str(count)) + str(post) + "<br><hr>")
    #     post_list.append("<small>" + str(post.body) + "<smell><br><br>")
    return render(request,"index.html",locals())


def showpost(request,slug):
    try:
        post = Post.objects.get(slug = slug )
        if post != None:
            return render(request,'post.html',locals())
    except:
        return redirect('/')

def author(request, author_id):
    dom="<h2> Here is author {} page</h2><hr>".format(author_id)
    return HttpResponse(dom)

from django.urls import reverse
def post_times(request, yr, mon, day, post_num):
    dom = "<h2> {}/{}/{}: POST #{}</h2><br><hr>".format(yr,mon,day, post_num)
    dom = dom + "<h2>Urls 反解析示範:</h2><br>"
    dom = dom + "{}<br>".format(reverse('post-url', args=("2020","12","1","003")))
    dom = dom +"<a href='{}'>超連結</a>".format(reverse('post-url', args=("2020","12","1","003")))
    return HttpResponse(dom)